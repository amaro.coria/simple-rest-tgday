package com.tecgurus.restsimple.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/demo")
public class DemoControllerRest {

    @Value(value = "${com.tecgurus.mensaje}")
    private String mensaje;

    //@GetMapping
    @RequestMapping(method = RequestMethod.GET)
    public String saluda(){
        return mensaje;
    }

}
