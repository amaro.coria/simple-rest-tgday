FROM openjdk:8-jre-alpine
COPY ./target/rest-simple-0.0.1-SNAPSHOT.jar /usr/share/app.jar
CMD [ "java" , "-jar" , "/usr/share/app.jar" ]